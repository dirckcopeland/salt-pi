base:
  '*':
    - defaults
    - nodes.prometheus_node_exporter # This registered the node_exporter as a service so it gets discovered
    - ssh

  # NOTE: These work to override as they're evaluated after the defaults above
  ######################
  #### Environments ####
  '*.ops.*':
    - envs.ops
  '*.vagrant.*':
    - nodes.vagrant
    - envs.vagrant
  '*.dev.*':
    - envs.dev
  '*.qa.*':
    - envs.qa
  '*.stage.*':
    - envs.stage
  '*.prod.*':
    - envs.prod

  ###############
  #### Nodes ####
  # Consul
  '*-consulserver*':
    - nodes.consulserver
