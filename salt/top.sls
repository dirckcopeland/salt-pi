base:
  # All Machines
  '*':
    - system.baseconfig

  ############### Vagrant Machines ###############
  '*-vagrantdev1.vagrant.*':
    - system.zookeeper.server
    - system.kafka.server
    - system.ndb_cluster.mgmt
    - system.ndb_cluster.data
    - system.ndb_cluster.sql
    - system.cassandra

  ############### Operations Machines ###############
  # Salt Server(s)
  '*-saltmaster*':
    - system.salt.master
